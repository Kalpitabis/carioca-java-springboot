package in.arakaki.carioca.controller;

import in.arakaki.carioca.business_rules.enterprise.User;

import in.arakaki.carioca.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/user")
public class CloudSQLController {

    @Autowired
    private UserService userService;

// TODO: implement find by id
//    @GetMapping(value = "/{sku}", produces = MediaType.APPLICATION_JSON_VALUE)
//    public Product find(@PathVariable("sku") Integer sku) {
//        return this.productService.find(sku);
//    }
// TODO: implement custom exceptions

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> list() {
        return this.userService.getUsers();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void save(@RequestBody User user) throws Exception {
        userService.saveUser(user);
    }

    @DeleteMapping(value = "/user")
    public void deletUser(@RequestBody User user){
        userService.deleteUser(user);
    }

    @PutMapping(value = "/user")
    public void update(@RequestBody User user){
        userService.saveUser(user);
    }
}
