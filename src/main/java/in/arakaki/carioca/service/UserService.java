package in.arakaki.carioca.service;

import in.arakaki.carioca.business_rules.enterprise.User;
import java.util.List;

public interface UserService {

    public List<User> getUsers();

    public User getUser(String name);

    public void saveUser(User user);

    public void deleteUser(User user);

}
